if (document.getElementById("main_map")) {
    var map = L.map('main_map').setView([6.2133079,-75.585012], 15);

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
    maxZoom: 18
    }).addTo(map);

    L.control.scale().addTo(map);

    /* L.marker([6.2152091,-75.5749966],{draggable: true}).addTo(map);
    L.marker([6.2150424,-75.5751173],{draggable: true}).addTo(map);
    L.marker([6.2136791,-75.5743147],{draggable: true}).addTo(map); */

    fetch('api/bicicletas')
        .then(response => response.json())
        .then(data => {
            console.log(data);
            data.bicicletas.forEach(bici => L.marker(bici.ubicacion,{draggable: true, title: bici.id}).addTo(map));
        });
}